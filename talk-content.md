## Introduction
### Goals
- Show what elm does well
- When you might not want to use it
- Refactoring in elm

### Agenda
- What is Elm?
- Feel for the code
- The Elm Architecture
- Refactoring example
- The life of a file (maybe?)

## What is Elm?
From the elm-lang.org website:
A delightful language for reliable webapps
Generate Javascript with great performance and **no runtime exceptions**

- Functional language
- Statically type + inference
- All data is immutable
- All function are pure (no side effects)
	- When you look at function you have these performance
- 'null' / 'undefined' / Exceptions do not exist

### Functional Programming
## Note about about semantic incorrectness vs broken
Just because there are no exceptions does not mean you can produce code that is wrong
"The sky is green" vs "The sky is undefined"

You can do functional programming with JS but it requires a lot of discipline 

## The Elm Architecture
Replaces: npm, webpack, react, redux, typescript, immutable.js
All built in into the language / "framework"

## References
(Elm Crash Course)[https://www.youtube.com/watch?v=kEitFAY7Gc8]